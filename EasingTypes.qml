import QtQuick 2.15
import QtQml.Models 2.15

ListModel {
    ListElement { value: Easing.Linear        ; text: "Linear"         ; image: "qeasingcurve-linear.png" }
    ListElement { value: Easing.InQuad        ; text: "In Quad"        ; image: "qeasingcurve-inquad.png" }
    ListElement { value: Easing.OutQuad       ; text: "Out Quad"       ; image: "qeasingcurve-outquad.png" }
    ListElement { value: Easing.InOutQuad     ; text: "In Out Quad"    ; image: "qeasingcurve-inoutquad.png" }
    ListElement { value: Easing.OutInQuad     ; text: "Out In Quad"    ; image: "qeasingcurve-outinquad.png" }
    ListElement { value: Easing.InCubic       ; text: "In Cubic"       ; image: "qeasingcurve-incubic.png" }
    ListElement { value: Easing.OutCubic      ; text: "Out Cubic"      ; image: "qeasingcurve-outcubic.png" }
    ListElement { value: Easing.InOutCubic    ; text: "In Out Cubic"   ; image: "qeasingcurve-inoutcubic.png" }
    ListElement { value: Easing.OutInCubic    ; text: "Out In Cubic"   ; image: "qeasingcurve-outincubic.png" }
    ListElement { value: Easing.InQuart       ; text: "In Quart"       ; image: "qeasingcurve-inquart.png" }
    ListElement { value: Easing.OutQuart      ; text: "Out Quart"      ; image: "qeasingcurve-outquart.png" }
    ListElement { value: Easing.InOutQuart    ; text: "In Out Quart"   ; image: "qeasingcurve-inoutquart.png" }
    ListElement { value: Easing.OutInQuart    ; text: "Out In Quart"   ; image: "qeasingcurve-outinquart.png" }
    ListElement { value: Easing.InQuint       ; text: "In Quint"       ; image: "qeasingcurve-inquint.png" }
    ListElement { value: Easing.OutQuint      ; text: "Out Quint"      ; image: "qeasingcurve-outquint.png" }
    ListElement { value: Easing.InOutQuint    ; text: "In Out Quint"   ; image: "qeasingcurve-inoutquint.png" }
    ListElement { value: Easing.OutInQuint    ; text: "Out In Quint"   ; image: "qeasingcurve-outinquint.png" }
    ListElement { value: Easing.InSine        ; text: "In Sine"        ; image: "qeasingcurve-insine.png" }
    ListElement { value: Easing.OutSine       ; text: "Out Sine"       ; image: "qeasingcurve-outsine.png" }
    ListElement { value: Easing.InOutSine     ; text: "In Out Sine"    ; image: "qeasingcurve-inoutsine.png" }
    ListElement { value: Easing.OutInSine     ; text: "Out In Sine"    ; image: "qeasingcurve-outinsine.png" }
    ListElement { value: Easing.InExpo        ; text: "In Expo"        ; image: "qeasingcurve-inexpo.png" }
    ListElement { value: Easing.OutExpo       ; text: "Out Expo"       ; image: "qeasingcurve-outexpo.png" }
    ListElement { value: Easing.InOutExpo     ; text: "In Out Expo"    ; image: "qeasingcurve-inoutexpo.png" }
    ListElement { value: Easing.OutInExpo     ; text: "Out In Expo"    ; image: "qeasingcurve-outinexpo.png" }
    ListElement { value: Easing.InCirc        ; text: "In Circ"        ; image: "qeasingcurve-incirc.png" }
    ListElement { value: Easing.OutCirc       ; text: "Out Circ"       ; image: "qeasingcurve-outcirc.png" }
    ListElement { value: Easing.InOutCirc     ; text: "In Out Circ"    ; image: "qeasingcurve-inoutcirc.png" }
    ListElement { value: Easing.OutInCirc     ; text: "Out In Circ"    ; image: "qeasingcurve-outincirc.png" }
    ListElement { value: Easing.InElastic     ; text: "In Elastic"     ; image: "qeasingcurve-inelastic.png" }
    ListElement { value: Easing.OutElastic    ; text: "Out Elastic"    ; image: "qeasingcurve-outelastic.png" }
    ListElement { value: Easing.InOutElastic  ; text: "In Out Elastic" ; image: "qeasingcurve-inoutelastic.png" }
    ListElement { value: Easing.OutInElastic  ; text: "Out In Elastic" ; image: "qeasingcurve-outinelastic.png" }
    ListElement { value: Easing.InBack        ; text: "In Back"        ; image: "qeasingcurve-inback.png" }
    ListElement { value: Easing.OutBack       ; text: "Out Back"       ; image: "qeasingcurve-outback.png" }
    ListElement { value: Easing.InOutBack     ; text: "In Out Back"    ; image: "qeasingcurve-inoutback.png" }
    ListElement { value: Easing.OutInBack     ; text: "Out In Back"    ; image: "qeasingcurve-outinback.png" }
    ListElement { value: Easing.InBounce      ; text: "In Bounce"      ; image: "qeasingcurve-inbounce.png" }
    ListElement { value: Easing.OutBounce     ; text: "Out Bounce"     ; image: "qeasingcurve-outbounce.png" }
    ListElement { value: Easing.InOutBounce   ; text: "In Out Bounce"  ; image: "qeasingcurve-inoutbounce.png" }
    ListElement { value: Easing.OutInBounce   ; text: "Out In Bounce"  ; image: "qeasingcurve-outinbounce.png" }
}
