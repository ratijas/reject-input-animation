import QtQuick 2.15
import QtQml 2.15
import QtQuick.Window 2.15

PathAnimation {
    id: root

    required property AnimationParams params

    alwaysRunToEnd: true

    duration: params.duration
    easing.type: params.easingType
    target: Item { id: item }

    property Binding __binding: Binding {
        when: root.running
        target: params.translation
        property: "x"
        value: item.x
        restoreMode: Binding.RestoreBindingOrValue
    }

    path: Path {
        PathPolyline {
            function repeat(arr, n) {
                return [].concat(...new Array(n).fill(arr));
            }

            function point(direction) {
                return Qt.point(params.swing * params.directionFactor * direction, 0);
            }

            property var pairs: {
                const pair = [
                    point(-1),
                    point(1),
                ];
                const pairs = repeat(pair, Math.floor(params.wiggleCount / 2));
                // add an ending if needed
                if (params.wiggleCount % 2 === 1) {
                    pairs.push(point(-1));
                }
                return pairs;
            }

            path: [
                Qt.point(0, 0),
                point(1),
                ...pairs,
                Qt.point(0, 0),
            ]
        }
    }
}
