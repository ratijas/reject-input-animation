import QtQuick 2.15
import QtQml 2.15
import QtQuick.Window 2.15

QtObject {
    id: root

    readonly property AnimationParams params: AnimationParams { id: params }

    readonly property Animation animation: switch (params.animationType) {
        case Enums.AnimationType.Sequential: return __sequential;
        case Enums.AnimationType.SinglePath: return __path;
    }

    property Animation __sequential: AnimationSequential { params: root.params }
    property Animation __path: AnimationSinglePath { params: root.params }

    property Instantiator __instantiator: Instantiator {
        active: root.params.animationTargetType === Enums.AnimationTargetType.Elements
        model: root.params.targets

        delegate: QtObject {
            id: delegate

            property Item item: modelData

            property Binding binding: Binding {
                target: delegate.item
                property: "enabled"
                value: false
                when: root.animation.running
                restoreMode: Binding.RestoreBindingOrValue
            }

            property Translate translate: Translate { x: params.translation.x }
        }

        onObjectAdded: (index, object) => {
            object.item.transform.push(object.translate);
        }

        onObjectRemoved: (index, object) => {
            object.item.transform = Array.prototype.slice.call(object.item.transform)
                .filter(item => item !== object.translate);
        }
    }

    property int __initialPositionX: 0
    property Binding __binding: Binding {
        when: params.animationTargetType === Enums.AnimationTargetType.Window && animation.running
        target: params.window
        property: "x"
        value: root.__initialPositionX + params.translation.x
        restoreMode: Binding.RestoreBindingOrValue
    }

    function reject() {
        if (params.window) {
            __initialPositionX = params.window.x;
        }
        animation.start();
    }
}
