import QtQuick 2.15
import QtQml 2.15
import QtQuick.Window 2.15

SequentialAnimation {
    id: root

    required property AnimationParams params
    readonly property int duration: params.duration / (params.wiggleCount + 2)

    alwaysRunToEnd: true

    NumberAnimation {
        targets: params.translation; property: "x"
        easing.type: params.startEndAreSpecial ? Easing.OutQuad : params.easingType
        duration: root.duration
        to: params.swing * params.directionFactor
    }

    SequentialAnimation {
        loops: Math.floor(params.wiggleCount / 2)

        NumberAnimation {
            targets: params.translation; property: "x"
            easing.type: params.easingType; duration: root.duration
            to: params.swing * params.directionFactor * -1
        }

        NumberAnimation {
            targets: params.translation; property: "x"
            easing.type: params.easingType; duration: root.duration
            to: params.swing * params.directionFactor
        }
    }

    NumberAnimation {
        property bool enabled: params.wiggleCount % 2 === 1
        targets: enabled ? params.translation : []
        property: "x"
        easing.type: params.easingType
        duration: enabled ? root.duration : 0
        to: params.swing * params.directionFactor * -1
    }

    NumberAnimation {
        targets: params.translation; property: "x"
        easing.type: params.startEndAreSpecial ? Easing.InQuad : params.easingType
        duration: root.duration
        to: 0
    }
}
