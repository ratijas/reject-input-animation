import QtQuick 2.15
import QtQml 2.15
import QtQuick.Window 2.15

QtObject {
    property int/*Enums.AnimationTargetType*/ animationTargetType: Enums.AnimationTargetType.Elements

    /** List of target Items for the `AnimationTargetType.Elements`. */
    property list<Item> targets

    /** Target window for the `AnimationTargetType.Window`. */
    property Window window

    // Helper for implementations.
    readonly property Translate translation: Translate {}

    property int/*Enums.AnimationType*/ animationType: Enums.AnimationType.Sequential

    // Helper for implementations.
    readonly property Item item: Item {}

    /** Total animation duration, in milliseconds. */
    property int duration: 1000

    /** Number of turns left and right. */
    property int wiggleCount: 5

    /** The magnitude/distance/offset of the animation, in the usual device-independent pixels. */
    property real swing: 20

    /** In which direction the target starts moving first.
        Must be either Qt.AlignLeft or Qt.AlignRight. */
    property int initialDirection: Qt.AlignLeft

    // helper for implementations
    readonly property int directionFactor: initialDirection === Qt.AlignLeft ? -1 : 1

    /** Easing curve for each segment or for the whole path at once. */
    property int/*Easing*/ easingType: Easing.OutQuad

    /** If set to true, easing type of the first and the last segments of a
        `AnimationType.Sequential` animation will be set to Out Quad and In Quad. */
    property bool startEndAreSpecial: false
}
