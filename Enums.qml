import QtQuick 2.0

QtObject {
    enum AnimationTargetType {
        Elements,
        Window
    }

    enum AnimationType {
        Sequential,
        SinglePath
    }
}
