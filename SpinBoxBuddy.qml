import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

SpinBox {
    required property Slider target
    required property string suffix

    from: target.from
    to: target.to
    value: target.value
    onValueModified: target.value = value
    Layout.preferredWidth: metrics.averageCharacterWidth * 12
    textFromValue: value => String(value) + suffix
    valueFromText: text => parseInt(text)
}
