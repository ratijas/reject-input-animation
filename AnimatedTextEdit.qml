import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtGraphicalEffects 1.15
import QtQml 2.15

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 3.0 as PlasmaComponents3
import org.kde.plasma.extras 2.0 as PlasmaExtras

TextField {
    id: control

    property real swing: height
    property int easingType: Easing.InOutQuad
    property int duration: 150
    property int wiggleCount: 3
    // Qt.AlignLeft or Qt.AlignRight
    property int initialDirection: Qt.AlignLeft
    property bool startEndAreSpecial: false

    property int __direction: initialDirection === Qt.AlignLeft ? -1 : 1

    onAccepted: rejectionAnimation.start()

    transform: Translate {
        id: translation
    }

    Binding {
        target: control
        property: "enabled"
        value: false
        when: rejectionAnimation.running
        restoreMode: Binding.RestoreBindingOrValue
    }

    SequentialAnimation {
        id: rejectionAnimation
        alwaysRunToEnd: true

        NumberAnimation {
            target: translation; property: "x"
            easing.type: startEndAreSpecial ? Easing.OutQuad : control.easingType
            duration: control.duration
            to: control.swing * control.__direction
        }
        SequentialAnimation {
            loops: Math.floor(control.wiggleCount / 2)

            NumberAnimation {
                target: translation; property: "x"
                easing.type: control.easingType; duration: control.duration
                to: control.swing * control.__direction * -1
            }
            NumberAnimation {
                target: translation; property: "x"
                easing.type: control.easingType; duration: control.duration
                to: control.swing * control.__direction
            }
        }
        NumberAnimation {
            property bool enabled: control.wiggleCount % 2 === 1
            target: enabled ? translation : null
            property: "x"
            easing.type: control.easingType
            duration: enabled ? control.duration : 0
            to: control.swing * control.__direction * -1
        }
        NumberAnimation {
            target: translation; property: "x"
            easing.type: startEndAreSpecial ? Easing.InQuad : control.easingType
            duration: control.duration
            to: 0
        }
    }
}
