import QtQuick 2.15
import QtQml 2.15

QtObject {
    id: root

    property url baseUrl

    // model with an 'image' string role
    property var model

    property Instantiator __instantiator: Instantiator {
        model: root.model

        delegate: Image {
            required property string image

            cache: true
            source: root.baseUrl + image
        }
    }
}
