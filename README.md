Animation constructor for "rejection"
=====================================

This is what everyone appears to using for password text inputs, and that's for a lot good reasons, and we should do it too.

The app is a simple scaffolded Qt/QML project, it can be build normally with a Qt Creator, or manually with cmake:

```sh
mkdir build && cd build
cmake .. && cmake --build
./reject-input-animation
```

All discussions and suggestions about the best parameters to use should go to [the issue #1](https://invent.kde.org/ratijas/reject-input-animation/-/issues/1).
