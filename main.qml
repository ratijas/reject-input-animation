import QtQuick 2.15
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QtQml.Models 2.15

ApplicationWindow {
    id: window

    // a little bit of autosizing
    width: contentItem.childrenRect.width + 2*18
    height: contentItem.childrenRect.height + 2*18
    visible: true
    title: qsTr("Animated rejection")

    flags: Qt.Dialog | Qt.WindowCloseButtonHint

    readonly property int/*Enums.AnimationTargetType*/ animationTargetType:
        animationTargetTypeGroup.checkedButton === animationTargetTypeElements
            ? Enums.AnimationTargetType.Elements
            : Enums.AnimationTargetType.Window

    readonly property int/*Enums.AnimationType*/ animationType:
        animationTypeGroup.checkedButton === animationTypeSequential
            ? Enums.AnimationType.Sequential
            : Enums.AnimationType.SinglePath

    property Rejection animation: Rejection {
        params {
            animationTargetType: window.animationTargetType
            targets: animationTargetIncludesButtonCheckbox.checked ? [passwordBox, actionButton] : [passwordBox]
            window: window

            animationType: window.animationType
            duration: speedSlider.value
            wiggleCount: wiggleCountSlider.value
            swing: swingSlider.value
            initialDirection: initialDirectionGroup.initialDirection
            easingType: easingSelector.currentValue
            startEndAreSpecial: startEndCheckbox.checked
        }
    }

    FontMetrics {
        id: metrics
        font: window.font
    }

    EasingTypes {
        id: types
    }

    GridLayout {
        anchors.centerIn: parent

        columns: 3
        columnSpacing: 8
        rowSpacing: 8

        TextField {
            id: passwordBox

            Layout.row: 0
            Layout.column: 1
            Layout.fillWidth: true

            placeholderText: "Hit " + actionButton.action.shortcut
            focus: true
            echoMode: TextInput.Password
        }

        Button {
            id: actionButton
            action: Action {
                icon.name: "go-next"
                shortcut: "Ctrl+R"
                onTriggered: {
                    passwordBox.selectAll();
                    window.animation.reject();
                }
            }
        }

        Label {
            Layout.row: 1
            Layout.alignment: Qt.AlignRight
            text: "Swing distance:"
        }

        Slider {
            id: swingSlider

            Layout.fillWidth: true

            from: 0
            to: 100
            value: 18
            stepSize: 1
            snapMode: Slider.NoSnap
        }

        SpinBoxBuddy {
            target: swingSlider
            suffix: " px"
        }

        Label {
            Layout.row: 2
            Layout.alignment: Qt.AlignRight
            text: "Duration:"
        }

        Slider {
            id: speedSlider

            Layout.fillWidth: true

            from: 100
            to: 2000
            value: 1000
            stepSize: 20
            snapMode: Slider.SnapAlways
        }

        SpinBoxBuddy {
            target: speedSlider
            suffix: " ms"
        }

        Label {
            Layout.row: 3
            Layout.alignment: Qt.AlignRight
            text: "Wiggle count:"
        }

        Slider {
            id: wiggleCountSlider

            Layout.fillWidth: true

            from: 1
            to: 10
            value: 5
            stepSize: 1
            snapMode: Slider.SnapAlways
        }

        SpinBoxBuddy {
            target: wiggleCountSlider
            // i18n on a budget
            suffix: value == 1 ? " time" : " times"
        }

        Label {
            Layout.row: 4
            Layout.alignment: Qt.AlignRight
            text: "Initial direction:"
        }

        ButtonGroup {
            id: initialDirectionGroup
            readonly property int initialDirection:
                checkedButton === leftDirectionButton ? Qt.AlignLeft : Qt.AlignRight
        }

        RowLayout {
            spacing: 4
            Layout.fillWidth: true

            RadioButton {
                id: leftDirectionButton
                ButtonGroup.group: initialDirectionGroup
                Layout.fillWidth: true
                Layout.preferredWidth: 1
                text: "Left"
                checked: true
            }

            RadioButton {
                ButtonGroup.group: initialDirectionGroup
                Layout.fillWidth: true
                Layout.preferredWidth: 1
                text: "Right"
            }
        }

        Label {
            Layout.row: 5
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            text: "Target:"
        }

        ButtonGroup {
            id: animationTargetTypeGroup
        }

        ColumnLayout {
            spacing: 4
            Layout.fillWidth: true

            RowLayout {
                spacing: 4
                Layout.fillWidth: true

                RadioButton {
                    id: animationTargetTypeElements
                    ButtonGroup.group: animationTargetTypeGroup
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: "Text Field"
                    checked: true
                }

                RadioButton {
                    ButtonGroup.group: animationTargetTypeGroup
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: "Window"
                }
            }

            CheckBox {
                id: animationTargetIncludesButtonCheckbox
                Layout.fillWidth: true
                Layout.leftMargin: 18
                text: "Animate button too"
                checked: false
                enabled: window.animationTargetType === Enums.AnimationTargetType.Elements
            }
        }

        Label {
            Layout.row: 6
            Layout.alignment: Qt.AlignRight | Qt.AlignTop
            text: "Animation type:"
        }

        ButtonGroup {
            id: animationTypeGroup
        }

        ColumnLayout {
            Layout.fillWidth: true

            RowLayout {
                Layout.fillWidth: true

                RadioButton {
                    id: animationTypeSequential
                    ButtonGroup.group: animationTypeGroup
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: "Sequential"
                }

                RadioButton {
                    ButtonGroup.group: animationTypeGroup
                    Layout.fillWidth: true
                    Layout.preferredWidth: 1
                    text: "Single Path"
                    checked: true
                }
            }

            CheckBox {
                id: startEndCheckbox
                Layout.fillWidth: true
                Layout.leftMargin: 18
                text: "Use Out/In Quad for Start/End"
                checked: false
                enabled: window.animationType === Enums.AnimationType.Sequential
            }
        }

        Label {
            Layout.row: 7
            Layout.column: 0
            Layout.alignment: Qt.AlignRight
            text: "Easing type:"
        }

        ComboBox {
            id: easingSelector

            Layout.fillWidth: true

            model: types
            textRole: "text"
            valueRole: "value"

            delegate: ItemDelegate {
                id: itemDelegate

                required text
                required property string image
                required property int index

                width: easingSelector.width
                contentItem: RowLayout {
                    spacing: 8
                    Image {
                        source: "https://doc.qt.io/qt-5/images/" + itemDelegate.image
                        Layout.preferredWidth: 64
                        Layout.preferredHeight: 64
                    }
                    Label {
                        text: itemDelegate.text
                        elide: Text.ElideRight
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignVCenter
                    }
                }
                highlighted: easingSelector.highlightedIndex === index
            }

            Component.onCompleted: currentIndex = indexOfValue(Easing.InOutQuad)
        }

        Image {
            id: image

            Layout.row: 8
            Layout.column: 1
            Layout.preferredHeight: 128
            Layout.preferredWidth: 128
            Layout.alignment: Qt.AlignHCenter

            source: "https://doc.qt.io/qt-5/images/" + types.get(easingSelector.currentIndex).image

            BusyIndicator {
                anchors.centerIn: parent
                running: image.status === Image.Loading
            }
        }
    }

    ImagePreloader {
        baseUrl: "https://doc.qt.io/qt-5/images/"
        model: types
    }
}
